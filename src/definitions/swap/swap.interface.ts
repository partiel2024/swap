import { IAccount } from "../accounts";

export interface ISwap {
    tokenA: string;
    tokenB: string;
    amountA: number;
    amountB: number;
    account: string | IAccount;
}