import { Router, Request, Response, json } from 'express';
import { Model, Mongoose } from 'mongoose';
import { ISwap } from '../../definitions/swap';
import { SwapSchema } from '../mongoose/schemas/swap.schema';

export class Authcontroller {

    private connection: Mongoose;
    private swapModel: Model<ISwap>;

    public constructor(connection: Mongoose) {
        this.connection = connection;
        this.swapModel = this.connection.model("Swap", SwapSchema);
    }

    buildRoutes(): Router {
        const router = Router();
        // router.post("/swap", json(), this.swap.bind(this));
        return router;
    }

}