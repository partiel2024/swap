import mongoose, { SchemaTypes } from "mongoose";
import { ISwap } from "../../../definitions/swap";

export const SwapSchema = new mongoose.Schema<ISwap>({
    tokenA: {
        type: SchemaTypes.String,
        required: true
    },
    tokenB: {
        type: SchemaTypes.String,
        required: true
    },
    amountA: {
        type: SchemaTypes.Number,
        required: true
    },
    amountB: {
        type: SchemaTypes.Number,
        required: true
    },
    account: {
        type: SchemaTypes.ObjectId,
        required: true,
        ref: "Account"
    }
}, {
    versionKey: false,
    collection: "sessions",
    timestamps: true
});